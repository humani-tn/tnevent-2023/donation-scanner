import pika
import os
import json
from time import sleep
from helloasso_api import ApiV5Client

print("Initialisation de la connexion à RabbitMQ")

exchange_name = os.getenv("AMQP_EXCHANGE", "notifications")

FETCH_INTERVAL = int(os.getenv("FETCH_INTERVAL", 10))

connection = pika.BlockingConnection(
    pika.ConnectionParameters(
        host=os.getenv("AMQP_HOST", "localhost"), port=os.getenv("AMQP_PORT", 5672)
    )
)
channel = connection.channel()
channel.exchange_declare(exchange=exchange_name, exchange_type="fanout")


def send_message(topic, message):
    global channel

    message = json.dumps(
        {
            "topic": topic,
            "message": message,
        }
    )
    channel.basic_publish(exchange=exchange_name, routing_key="", body=message)
    print("> Sent %r" % message)


def get_info(idx, don):
    if "payer" in don:
        return {
            "id": idx,
            "name": f"{don['payer']['firstName']} {don['payer']['lastName'][0]}.",
            "amount": don["amount"],
        }
    else:
        return {"id": idx, "name": "Anonyme", "amount": don["amount"]}


def derniers_donateurs(dons, n):
    return dons[-n:]


print("Initialisation de la connexion à HelloAsso")

api = ApiV5Client(
    api_base="api.helloasso.com",
    client_id=os.getenv("HA_CLIENT_ID", ""),
    client_secret=os.getenv("HA_CLIENT_SECRET", ""),
    timeout=60,
)


organization = os.getenv("HA_ORGANIZATION", "")
formtype = os.getenv("HA_FORMTYPE", "Form")
formslug = os.getenv("HA_FORM_SLUG", 1)

TOPIC = os.getenv("TOPIC", "/dons")

all_data = []

total = 0

print("Initialisation des dons")

response = api.call(
    f"/v5/organizations/{organization}/forms/{formtype}/{formslug}/payments?pageSize=100&sortOrder=Asc",
    method="GET",
)

json_data = response.json()
if "continuationToken" not in json_data["pagination"]:
    print("Pas de dons")
    send_message(f"{TOPIC}/total", {"total": total, "count": len(all_data)})
    send_message(
        f"{TOPIC}/derniers", {"derniers": list(derniers_donateurs(all_data, 10))}
    )
    sleep(10)

while True:
    json_data = response.json()
    if "continuationToken" in json_data["pagination"]:
        break
    print("Pas de dons")

    response = api.call(
        f"/v5/organizations/{organization}/forms/{formtype}/{formslug}/payments?pageSize=100&sortOrder=Asc",
        method="GET",
    )
    sleep(FETCH_INTERVAL)

print("Au moins un don")

while True:

    json_data = response.json()
    continue_token = json_data["pagination"]["continuationToken"]

    count = json_data["pagination"]["totalCount"]

    filtered = filter(lambda x: x["state"] == "Authorized", json_data["data"])
    new_data = map(lambda x: get_info(x[0] + len(all_data), x[1]), enumerate(filtered))
    new_data = list(new_data)
    total += sum(map(lambda x: x["amount"], new_data))
    all_data.extend(new_data)

    if count == 0:
        break

    print("Dons à récupèrer : ", count)
    response = api.call(
        f"/v5/organizations/{organization}/forms/{formtype}/{formslug}/payments?pageSize=100&continuationToken={continue_token}&sortOrder=Asc",
        method="GET",
    )

print(f"Total : {total/100}€")
send_message(f"{TOPIC}/total", {"total": total, "count": len(all_data)})
send_message(f"{TOPIC}/derniers", {"derniers": list(derniers_donateurs(all_data, 10))})
print("En attente de nouveaux dons")

c = 0
while True:
    json_data = response.json()
    continue_token = json_data["pagination"]["continuationToken"]

    count = json_data["pagination"]["totalCount"]

    filtered = filter(lambda x: x["state"] == "Authorized", json_data["data"])
    new_data = map(lambda x: get_info(x[0] + len(all_data), x[1]), enumerate(filtered))
    new_data = list(new_data)
    total += sum(map(lambda x: x["amount"], new_data))
    all_data.extend(new_data)

    c = c + 1

    if c % 60 == 0:
        print("Rafraichissement du token")
        api.oauth.refresh_tokens()

    if count > 0:
        print("Nouveau don(s) !")
        send_message(f"{TOPIC}/total", {"total": total, "count": len(all_data)})
        send_message(
            f"{TOPIC}/derniers", {"derniers": list(derniers_donateurs(all_data, 10))}
        )
    else:
        print("Pas de nouveau dons")
    sleep(FETCH_INTERVAL)
    response = api.call(
        f"/v5/organizations/{organization}/forms/{formtype}/{formslug}/payments?pageSize=100&continuationToken={continue_token}&sortOrder=Asc",
        method="GET",
    )
