# Donation scanner


## Usage

Create a `.env` file with the following variables:

```txt
AMQP_EXCHANGE=notifications
AMQP_HOST=localhost
AMPQ_PORT=5672
HA_CLIENT_ID=id
HA_CLIENT_SECRET=secret
HA_ORGANIZATION=orgname
HA_FORM_SLUG=1
```
